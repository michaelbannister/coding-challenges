## Prerequisites

* Java SDK version 8 or higher

## Build and test
To build and run tests, navigate into the project directory in a command window and run:

```bash
# macOS/Linux
./gradlew build

# Windows (not tested)
gradle.bat build
```